import time

import os

import sys

from subprocess import Popen, PIPE, STDOUT

if len(sys.argv) < 3:

   sys.exit('usage: contest3 refree player')

max_loop = 100

width = 5

height = 5

num_char = 5

refree_pipe = Popen([sys.argv[1], str(width), str(height), str(num_char)],

                   stdin=PIPE, stdout=PIPE, bufsize=1)

player_pipe = Popen([sys.argv[2]], stdin=PIPE, stdout=PIPE, bufsize=1)

format_str = str(width) + ' ' + str(height) + ' ' + str(num_char)

player_pipe.stdin.write(format_str + '\n')

loop = 1

while loop <= max_loop:

   for i in range(0, height):

       board_line = refree_pipe.stdout.readline()

       player_pipe.stdin.write(board_line + '\n')

   move = player_pipe.stdout.readline()

   print("[" + str(loop) + "] move: " + move)

   refree_pipe.stdin.write(move + '\n')

   score = refree_pipe.stdout.readline()

   print("[" + str(loop) + "] score: " + score)

   player_pipe.stdin.write(score + '\n')

   time.sleep(0.1)

   loop = loop + 1

player_pipe.kill()

player_pipe.wait()

refree_pipe.kill()

refree_pipe.wait()

sys.exit()
